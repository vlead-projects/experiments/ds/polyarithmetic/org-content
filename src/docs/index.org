#+title:  Documents of our project 
#+AUTHOR: VLEAD
#+DATE: [2018-05-14 Mon]
#+SETUPFILE: ../../org-templates/level-2.org
#+TAGS: boilerplate(b)
#+EXCLUDE_TAGS: boilerplate
#+OPTIONS: ^:nil

* Introduction
 Here are the required documents for our project:


|--------+---------------------------------------------------------------------------------------------+-----------|
| *SNo.* | *Purpose*                                                                                   | *Link*    |
|--------+---------------------------------------------------------------------------------------------+-----------|
|      1 | Resources like concepts,quizzes,animations etc useful for building the experiment structure | [[./collecteddata.org][Resources]] |
|--------+---------------------------------------------------------------------------------------------+-----------|
|      2 | Mapping of our experiment to Gagne nine level of learning                                   | [[./mapping.org][Mapping]]   |
|--------+---------------------------------------------------------------------------------------------+-----------|


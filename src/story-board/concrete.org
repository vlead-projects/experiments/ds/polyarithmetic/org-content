#+TITLE: Concrete Plan
#+AUTHOR: VLEAD
#+DATE: [2018-05-22 Thu]
#+SETUPFILE: ../org-templates/level-1.org
#+TAGS: boilerplate(b)
#+EXCLUDE_TAGS: boilerplate
#+OPTIONS: ^:nil

* Experiment Id 						     :expId:
  :PROPERTIES:
  :SCAT: expId
  :CUSTOM_ID: polyarithmetic
  :END:


* Type of Document                                                   :docType:
  :PROPERTIES:
  :SCAT: docType
  :CUSTOM_ID: A2
  :END:


* Introduction Module: 						     :module:
  :PROPERTIES:
  :SCAT: module
  :CUSTOM_ID: mod-intro
  :END:

** Structure of the Experiment 					       :scene:
  :PROPERTIES:
  :SCAT: scene
  :CUSTOM_ID: intro-sc-1
  :END:

*** Basic Introduction 						       :txta:
  :PROPERTIES:
  :SCAT: txta
  :CUSTOM_ID: basic-intro
  :END:
View the structure of the experiment given below.

This experiment begins with the basic introduction
of all the concepts and definitions related to the experiment.
There is a pre-test after introduction which is compulsory.

*** Structure of Experiment 					       :txta:
  :PROPERTIES:
  :SCAT: txta
  :CUSTOM_ID: exp-structure
  :END:
This experiment has the following modules:
- Module-1 :: Introduction
- Module-2 :: Linked Lists
- Pre-Test
- Module-3 :: Polynomial Introduction & Representation
- Module-4 :: Addition
- Module-5 :: Subtraction
- Module-6 :: Multiplication
- Module-7 :: Application of polynomials using linked lists
- Post-assessment

*** Weightage of Each Module 					       :txta:
  :PROPERTIES:
  :SCAT: txta
  :CUSTOM_ID: exp-weightage
  :END:
| Module                    | Weightage | Expectation                                  |
| Introduction              |       10% | To be read and understood                    |
| Linked Lists              |       10% | To be read and understood                    |
| Pre-Test                  |       10% | Answer all the question                      |
| Polynomial Representation |       15% | Understand the Represnetation                |
| Addition                  |       10% | Understand the addition operation            |
| Subtraction               |       10% | Understand the subtraction operation         |
| Multiplication            |       10% | Understand the multiplication operation      |
| Application               |        5% | Applications related                         |
| Post-assessment           |       20% | Solve All Questions                          |

** Learning Objectives 						       :scene:
  :PROPERTIES:
  :SCAT: scene
  :CUSTOM_ID: intro-sc-2
  :END:

*** Caption                                                            :txta:
  :PROPERTIES:
  :SCAT:     txta
  :CUSTOM_ID: learning-obj
  :END:
What will you do in this experiment?

*** Objectives                                                         :txta:
  :PROPERTIES:
  :SCAT:     txta
  :CUSTOM_ID: learning-obj
  :END:
In this experiment of Polynomial Arithmetic Using Linked Lists,
you will be able to do the following:
- Demonstrate understaing of the concepts of
  why we are using linkedlists to represent
  polynomial equation.
- Given a Polynomial, perform the basic arithmetic
  operation like addition subtraction and multipilcation.

** Prerequisites to learn Polynomial Arithmetic 		       :scene:
  :PROPERTIES:
  :SCAT: scene
  :CUSTOM_ID: intro-sc-3
  :END:

*** Intro							     :txta:
  :PROPERTIES:
  :SCAT: txta
  :CUSTOM_ID: intro-pre-req
  :END:
Before you learn about polynomials using linked lists,it
is important for the user to understand about polynomials
and linked lists. Click on the links below to learn these
concepts.

*** Links 							     :txta:
  :PROPERTIES:
  :SCAT: txta
  :CUSTOM_ID: pre-req-links
  :END:
- Click on this link to learn more about Polynomial Arithmetic
[[https://en.wikipedia.org/wiki/Polynomial_arithmetic][Polynomial-Arithmetic]]

- Click on this link to learn more about multiplication of polynomial using linked lists
 [[https://www.geeksforgeeks.org/multiply-two-polynomials-2/][Multiplication-Linkedlists]]



* Linked List Module : 						     :module:
  :PROPERTIES:
  :SCAT: module
  :CUSTOM_ID: mod-linkedlists
  :END:

** Linked List Introduction 					      :scene:
  :PROPERTIES:
  :SCAT: scene
  :CUSTOM_ID: intro-linkedlists-sc
  :END:

*** Introduction of Linked lists 				       :txta:
  :PROPERTIES:
  :SCAT: txta
  :CUSTOM_ID: linkedlists-intro
  :END:
A linked list is a linear collection of data elements.

*** Picture of Linked List 					       :reqa:
  :PROPERTIES:
  :CUSTOM_ID: linked-list-example-pic
  :REFERENCE_ID: linkedlists-intro
  :SCAT: reqa
  :TYPE: Image
  :END:
Provide an image illustrating the singly linked lists

*** Properties of Linked lists used                                    :txta:
  :PROPERTIES:
  :SCAT: txta
  :CUSTOM_ID: linkedlist-intro-properties
  :END:
- Insertion at the end of the list
- Iteration over list


* Pre-test Module: 						     :module:
  :PROPERTIES:
  :SCAT: module
  :CUSTOM_ID: mod-pre-test
  :END:

** Introduction to pre-test 					       :scene:
  :PROPERTIES:
  :SCAT: scene
  :CUSTOM_ID: pre-test-intro-sc
  :END:

*** Introduction                                                       :txta:
  :PROPERTIES:
  :SCAT: txta
  :CUSTOM_ID: pre-test-intro
  :END:
There are five questions in the test,each question can have one or more correct answers.
Click the most suitable options for each of the questions. Once you are done,click submit to finish the test.

** Pre-test question 1 						       :scene:
  :PROPERTIES:
  :SCAT: scene
  :CUSTOM_ID: pre-sc-1
  :END:

*** Question 							       :txta:
  :PROPERTIES:
  :SCAT: txta
  :CUSTOM_ID: pre-q1-ques
  :END:
Which of the following points is/are true about Linked List data structure when it is compared with array?

*** Options 							       :txta:
  :PROPERTIES:
  :CUSTOM_ID: pre-q1-options
  :SCAT: txta
  :END:
(A) Arrays have better cache locality that can make them better in terms of performance.
(B) It is easy to insert and delete elements in Linked List
(C) Random access is not allowed in a typical implementation of Linked Lists
(D) The size of array has to be pre-decided, linked lists can change their size any time.
(E) All of the above

*** Answer 							       :reqa:
  :PROPERTIES:
  :SCAT: reqa
  :CUSTOM_ID: pre-q1-answer
  :END:
Correct answer : (E)
Explanation : The size of the arrays is fixed: So we must know the upper limit on the number of elements in advance. Also, generally, the allocated memory is equal to the upper limit irrespective of the usage, and in practical uses, upper limit is rarely reached.
Check the answer and depending on whether the answer is correct or wrong, display the following:-
- Correct :: "Correct! Display(Explanation)"
- Wrong   :: "Wrong Answer". Display(Correct Answer). Display(Explanation)

** Pre-test question 2 						       :scene:
  :PROPERTIES:
  :SCAT: scene
  :CUSTOM_ID: pre-sc-2
  :END:

*** Question 							       :txta:
  :PROPERTIES:
  :SCAT: txta
  :CUSTOM_ID: pre-q2-ques
  :END:
What is the proper code for accessing the information of the second item in a linked list?

*** Options 							       :txta:
  :PROPERTIES:
  :CUSTOM_ID: pre-q2-options
  :SCAT: txta
  :END:
(A) Head.info
(B) Head.link.info
(C) Head.link.link.info
(D) None of these

*** Answer 							       :reqa:
  :PROPERTIES:
  :SCAT: reqa
  :CUSTOM_ID: pre-q1-answer
  :END:
Correct answer : (B)
Explanation : Head points to the first element of a list
Check the answer and depending on whether the answer is correct or wrong, display the following:-
- Correct :: "Correct! Display(Explanation)"
- Wrong   :: "Wrong Answer". Display(Correct Answer). Display(Explanation)

** Pre-test question 3 						       :scene:
  :PROPERTIES:
  :SCAT: scene
  :CUSTOM_ID: pre-sc-3
  :END:

*** Question 							       :txta:
  :PROPERTIES:
  :SCAT: txta
  :CUSTOM_ID: pre-q3-ques
  :END:
Which of the following sorting algorithms can be used to sort a random linked list with minimum time complexity?

*** Options 							       :txta:
  :PROPERTIES:
  :CUSTOM_ID: pre-q3-options
  :SCAT: txta
  :END:
(A) Insertion Sort
(B) Quick Sort
(C) Heap Sort
(D) Merge Sort

*** Answer 							       :reqa:
  :PROPERTIES:
  :SCAT: reqa
  :CUSTOM_ID: pre-q3-answer
  :END:
Correct answer : (D)
Explanation : Both Merge sort and Insertion sort can be used for linked lists. The slow random-access performance of a linked list makes other algorithms (such as quicksort) perform poorly, and others (such as heapsort) completely impossible. Since worst case time complexity of Merge Sort is O(nLogn) and Insertion sort is O(n^2), merge sort is preferred.
Check the answer and depending on whether the answer is correct or wrong, display the following:-
- Correct :: "Correct! Display(Explanation)"
- Wrong   :: "Wrong Answer". Display(Correct Answer). Display(Explanation)

** Pre-test question 4 						       :scene:
  :PROPERTIES:
  :SCAT: scene
  :CUSTOM_ID: pre-sc-4
  :END:

*** Question 							       :txta:
  :PROPERTIES:
  :SCAT: txta
  :CUSTOM_ID: pre-q4-ques
  :END:
Consider a single linked list where the pointers to the first and last element are known. The time for performing which of the given operations depends on the length of the

*** Options 							       :txta:
  :PROPERTIES:
  :CUSTOM_ID: pre-q4-options
  :SCAT: txta
  :END:
(A) Delete the first element of the list
(B) Interchange the first two elements of the list
(C) Delete the last element of the list
(D) Add an element at the end of the list

*** Answer 							       :reqa:
  :PROPERTIES:
  :SCAT: reqa
  :CUSTOM_ID: pre-q4-answer
  :END:
Correct answer : (C)
Explanation : Deleting the last element of the list will require the length traversal of the list so as to obtain the pointer of the node previous to the last node.
Check the answer and depending on whether the answer is correct or wrong, display the following:-
- Correct :: "Correct! Display(Explanation)"
- Wrong   :: "Wrong Answer". Display(Correct Answer). Display(Explanation)

** Pre-test question 5 						       :scene:
  :PROPERTIES:
  :SCAT: scene
  :CUSTOM_ID: pre-sc-5
  :END:

*** Question 							       :txta:
  :PROPERTIES:
  :SCAT: txta
  :CUSTOM_ID: pre-q5-ques
  :END:
The following function reverse() is supposed to reverse a singly linked list. There is one line missing at the end of the function.
- /* Link list node */
- struct node
- {
-    int data;
-    struct node* next;
- };

- /* head_ref is a double pointer which points to head (or start) pointer of linked list */
- static void reverse(struct node** head_ref)
- {
-    struct node* prev   = NULL;
-    struct node* current = *head_ref;
-    struct node* next;
-    while (current != NULL)
-    {
-        next  = current->next;
-        current->next = prev;
-        prev = current;
-        current = next;
-    }
-    /*ADD A STATEMENT HERE*/
- }
What should be added in place of "/*ADD A STATEMENT HERE*/", so that the function correctly reverses a linked list?

*** Options 							       :txta:
  :PROPERTIES:
  :CUSTOM_ID: pre-q5-options
  :SCAT: txta
  :END:
(A) *head_ref = prev;
(B) *head_ref = current;
(C) *head_ref = next;
(D) *head_ref = NULL;

*** Answer 							       :reqa:
  :PROPERTIES:
  :SCAT: reqa
  :CUSTOM_ID: pre-q5-answer
  :END:
Correct answer : (A)
Explanation : *head_ref = prev; At the end of while loop,
the prev pointer points to the last node of original linked list.
We need to change *head_ref so that the head pointer now starts pointing to the last node.
Check the answer and depending on whether the answer is correct or wrong, display the following:-
- Correct :: "Correct! Display(Explanation)"
- Wrong   :: "Wrong Answer". Display(Correct Answer). Display(Explanation)

** Pre Test Results 						       :scene:
  :PROPERTIES:
  :SCAT: scene
  :CUSTOM_ID: pre-test-results
  :END:
Compute the score based on answers from the quiz and display the score
to the user.

*** Pre test feedback						       :reqa:
  :PROPERTIES:
  :SCAT: reqa
  :CUSTOM_ID: pre-test-feedback
  :END:
Display the related feedback based on the score obtained in the pretest.
Assuming that there are 5 questions.

*** Further references 						       :txta:
  :PROPERTIES:
  :SCAT: txta
  :CUSTOM_ID: feedback-ref
  :END:
In case you still want to review these concepts, here
are links to some resources:
+ Linked List Basics
   - [[https://www.youtube.com/watch?v=njTh_OwMljA&t=1s][Youtube]]
   - [[https://www.youtube.com/watch?v=xiIoa2rfAaQ][Youtube]]
   - [[https://www.youtube.com/watch?v=_jQhALI4ujg][Youtube]]
   - [[https://www.cs.cmu.edu/~adamchik/15-121/lectures/Linked%20Lists/linked%20lists.html][Concept]]
Click the links to access these resources.


* Polynomial Introduction 					     :module:
  :PROPERTIES:
  :SCAT: module
  :CUSTOM_ID: mod-poly-intro
  :END:

** Introduction to Polynomials 					       :scene:
  :PROPERTIES:
  :SCAT: scene
  :CUSTOM_ID: poly-intro-sc
  :END:

*** Intro                                                             :txta:
  :PROPERTIES:
  :SCAT: txta
  :CUSTOM_ID: poly-intro
  :END:
/simple definition/
In mathematics, a polynomial is an expression consisting of variables and coefficients, that involves only the operations of addition, subtraction, multiplication, and non-negative integer exponents of variables.
An example of a polynomial of a single indeterminate x is x^2 − 4x + 7. An example in three variables is x^3 + 2xyz^2 − yz + 1.

** Picture representation 					       :scene:
  :PROPERTIES:
  :CUSTOM_ID: polynomial-example-pic-sc
  :SCAT: scene
  :END:

*** Picture of Polynomial 					       :reqa:
  :PROPERTIES:
  :CUSTOM_ID: polynomial-example-pic
  :TYPE: Image
  :SCAT: reqa
  :REFERNCE_ID: poly-intro
  :END:
This picture shows what a polynomial looks like.

** Quiz: 							       :scene:
  :PROPERTIES:
  :SCAT: scene
  :CUSTOM_ID: poly-quiz
  :END:

*** Question 							       :quiz:
  :PROPERTIES:
  :CUSTOM_ID: quiz
  :SCAT: quiz
  :END:

*** Header 							       :quizhead:
  :PROPERTIES:
  :CUSTOM_ID: quiz-head
  :SCAT: quizhead
  :END:
Select the options to answer the question and click on the
Submit button.

*** Question 1 							       :quizquest:
  :PROPERTIES:
  :CUSTOM_ID: quiz-q1
  :SCAT: quizquest
  :END:
Which expression is not a polynomial?


*** Options                                                            :txta:
  :PROPERTIES:
  :CUSTOM_ID: quiz-q1-options
  :SCAT: txta
  :END:
(A) (x^3)−2(x^2)+3x−2
(B)  −3x+5(x^14)−3
(C) (x^−2)+x
(D) 5


*** Answer                                                             :reqa:
  :PROPERTIES:
  :CUSTOM_ID: quiz-q1-options
  :SCAT: reqa
  :TYPE: InteractiveJS
  :END:
Have a check answer button.
Correct Answer : (C)
Explanation : Because of the neative power on X
Check the answer and depending on whether the answer is correct or wrong, display the following:-
- Correct :: "Correct!" Display(Explanation)"
- Wrong :: "Wrong Answer." Display(Correct Answer). Display(Explanation)


*** Next Question Button 					       :reqa:
  :PROPERTIES:
  :SCAT: reqa
  :CUSTOM_ID: quiz-next-q-button
  :END:
A button. On clicking this, the next question is shown.

*** Question 2 							       :quizquest:
  :PROPERTIES:
  :CUSTOM_ID: quiz-q2
  :SCAT: quizquest
  :END:
The degree of polynomial: 2(x^3)(y)−5(x^2)(y^3)−10xy+9x is?

*** Options                                                            :txta:
  :PROPERTIES:
  :CUSTOM_ID: quiz-q2-options
  :SCAT: txta
  :END:
(A) 0
(B) 1
(C) 4
(D) 5

*** Answer                                                             :reqa:
  :PROPERTIES:
  :CUSTOM_ID: quiz-q1-options
  :SCAT: reqa
  :TYPE: InteractiveJS
  :END:
Have a check answer button.
Correct Answer : (D)
Explanation - On adding the exponents of X and Y for the highest power
Check the answer and depending on whether the answer is correct or wrong, display the following:-
- Correct :: "Correct!" Display(Explanation)"
- Wrong :: "Wrong Answer." Display(Correct Answer). Display(Explanation)

*** Submit Button 						       :reqa:
  :PROPERTIES:
  :SCAT: reqa
  :CUSTOM_ID: poly-quiz-submit-button
  :END:
A button. On clicking this, the question get submited and Score appear
** Polynomial Represenation Using Linked lists 			       :scene:
  :PROPERTIES:
  :SCAT: scene
  :CUSTOM_ID: poly-rep-intro
  :END:

*** Steps Involved 						       :txta:
  :PROPERTIES:
  :SCAT: txta
  :CUSTOM_ID: poly-rep-steps
  :END:
1. When you look at a linked lists there are THREE BOX which appear
2. The First two boxes from left to right represent NODES which contain the coefficient part in the beginning and followed by the exponent part
3. The last left over box is called as TAIL and it contains pointer.

*** Pictorial Representation 					       :reqa:
  :PROPERTIES:
  :CUSTOM_ID: representation-pic
  :SCAT: reqa
  :TYPE: Image
  :END:
This picture represents how the above mentioned steps are to be followed

*** Demo of Representation                                             :reqa:
  :PROPERTIES:
  :CUSTOM_ID: representation-demo
  :REFERENCE_ID: representation
  :SCAT: reqa
  :TYPE: InteractiveJS
  :END:
Represenation of polynomial using linked lists

*** Represenation Exercise                                             :reqa:
  :PROPERTIES:
  :CUSTOM_ID: representation-artifact
  :REFERENCE_ID: representation
  :SCAT: reqa
  :TYPE: InteractiveJS
  :END:


* Polynomial operations: 					     :module:
  :PROPERTIES:
  :SCAT: module
  :CUSTOM_ID: mod-polynomial-operation
  :END:

** Opearations using linked list 				      :scene:
  :PROPERTIES:
  :SCAT: scene
  :CUSTOM_ID: polynomial-airthmetic-intro-sc
  :END:
All the elementary operations like addition subraction and
multiplication will be discussed below


* Addition 							     :module:
  :PROPERTIES:
  :SCAT: module
  :CUSTOM_ID: addition-intro-mod
  :END:
/simple definition/
Imagine to align the like terms vertically and then add them to get the result

** Addition Rep & Artifacts 					      :scene:
  :PROPERTIES:
  :SCAT: scene
  :CUSTOM_ID: addition-intro-mod-sc
  :END:

*** Picture of addition 						   :reqa:
  :PROPERTIES:
  :CUSTOM_ID: addition-example-pic
  :SCAT: reqa
  :TYPE: Image
  :END:
An example of addition using linked list

*** Addition Demo                                                          :reqa:
  :PROPERTIES:
  :CUSTOM_ID: addition-demo
  :REFERENCE_ID: addtion-example-pic
  :SCAT: reqa
  :TYPE: InteractiveJS
  :END:
Demo of how to add two polynomials using linked lists

*** Addition artefact 							   :reqa:
  :PROPERTIES:
  :CUSTOM_ID: addition-artifact
  :REFERENCE_ID: addtion-example-pic
  :SCAT: reqa
  :TYPE: InteractiveJS
  :END:
Two polynomials will be given to add. User have to represent
two polynomials in linked list form and then have to add
them in linked list manifestation


* Subtraction 							     :module:
  :PROPERTIES:
  :SCAT: module
  :CUSTOM_ID: subtraction-intro-mod
  :END:
/simple definition/
Imagine to align the like terms vertically and then subtract them to get the result

** Subtraction Rep & Artifacts                                        :scene:
  :PROPERTIES:
  :SCAT: module
  :CUSTOM_ID: subtraction-intro-mod-sc
  :END:

*** Picture of subtraction 						   :reqa:
  :PROPERTIES:
  :CUSTOM_ID: subtraction-example-pic
  :REFERENCE_ID: addtion-example-pic
  :SCAT: reqa
  :TYPE: Image
  :END:
An example of subtraction using linked list

*** Subtraction Demo                                                       :reqa:
  :PROPERTIES:
  :CUSTOM_ID: subtraction-demo
  :REFERENCE_ID: addtion-example-pic
  :SCAT: reqa
  :TYPE: InteractiveJS
  :END:
Demo of how to subtract two polynomials using linked lists

*** Subtraction artefact 						   :reqa:
  :PROPERTIES:
  :CUSTOM_ID: subtraction-artifact
  :REFERENCE_ID: addtion-example-pic
  :SCAT: reqa
  :TYPE: InteractiveJS
  :END:
Two polynomials will be given to subtract. User have to represent two
polynomials in linked list form and then have to subtract
them in linked list manifestation


* Multiplication 						     :module:
  :PROPERTIES:
  :SCAT:     module
  :CUSTOM_ID: multiplication-intro-mod
  :END:
/simple definition/

** Multiplication Rep & Artifacts 				      :scene:
  :PROPERTIES:
  :SCAT: module
  :CUSTOM_ID: multiplication-intro-mod-sc
  :END:

*** Picture of multiplication 						   :reqa:
  :PROPERTIES:
  :CUSTOM_ID: multiplication-example-pic
  :REFERENCE_ID: addtion-example-pic
  :SCAT: reqa
  :TYPE: Image
  :END:
An example of multiplication using linked list

*** Multiplication Demo                                                    :reqa:
  :PROPERTIES:
  :CUSTOM_ID: multiplication-demo
  :REFERENCE_ID: addtion-example-pic
  :SCAT: reqa
  :TYPE: InteractiveJS
  :END:
Demo of how to multiply two polynomials using linked lists

*** Multiplication artefact 						   :reqa:
  :PROPERTIES:
  :CUSTOM_ID: multiplication-artifact
  :REFERENCE_ID: addtion-example-pic
  :SCAT: reqa
  :TYPE: InteractiveJS
  :END:
Two polynomials will be given to multiply. User have to represent two
polynomials in linked list form and then have to multiply
them in linked list manifestation


* Post-test: 							     :module:

  :PROPERTIES:
  :SCAT: module
  :CUSTOM_ID: post-test-mod
  :END:

** Post-Test-Introduction					      :scene:
  :PROPERTIES:
  :SCAT: scene
  :CUSTOM_ID: post-test-introduction
  :END:

*** Intro							       :txta:
  :PROPERTIES:
  :SCAT: txta
  :CUSTOM_ID: post-test-intro
  :END:
   There are ten questions in the test,each question can have one or more correct answers..
   Click the most suitable options for each of the questions.Once you are done,click submit to finish the test.

{Quiz_header} Select the options to answer the question and
click on the Submit button.


** Post-test question 1 					      :scene:
  :PROPERTIES:
  :SCAT: scene
  :CUSTOM_ID: post-sc-1
  :END:

*** Question 							       :txta:
  :PROPERTIES:
  :SCAT: txta
  :CUSTOM_ID: post-q1-ques
  :END:
Which expression is a polynomial?

*** Options                                                            :txta:
  :PROPERTIES:
  :CUSTOM_ID: post-q1-options
  :SCAT:     txta
  :END:
a)  (x^2)-(x^1)+1-(x^-1)
b)  pi(x^2)+ e(x)
c)  5(x^.5) + 2(x^2)
d) (5^.5)(x^3)


*** Answer 							       :reqa:
  :PROPERTIES:
  :CUSTOM_ID: post-q1-options
  :SCAT:     reqa
  :TYPE:     Text
  :END:
Correct Answer : (D)
Explanation: Polynomials in one variable are algebraic expressions that consist of terms in the form a(x^n) where n is a non-negative (i.e. positive or zero) integer and a is a real number.
Check the answer and depending on whether the answer is correct or wrong, display the following:-
- Correct :: "Correct! Display(Explanation)"
- Wrong :: "Wrong Answer. Display(Correct Answer). Display(Explanation)"

** Post-test question 2 					      :scene:
  :PROPERTIES:
  :SCAT: scene
  :CUSTOM_ID: post-sc-2
  :END:

*** Question 							       :txta:
  :PROPERTIES:
  :SCAT: txta
  :CUSTOM_ID: post-q2-ques
  :END:
Two polynomial each of degree 4 are subtracrted,the degree of the difference could not  be?

*** Options                                                            :txta:
  :PROPERTIES:
  :CUSTOM_ID: post-q2-options
  :SCAT:     txta
  :END:
a) 0
b) 5
c) 4
d) 3

*** Answer                                                             :reqa:
  :PROPERTIES:
  :CUSTOM_ID: post-q2-options
  :SCAT: reqa
  :TYPE: Text
  :END:
Correct Answer : (B)
Explanation: Degree of the difference could be atmost equal to the degree of given polynomials
Check the answer and depending on whether the answer is correct or wrong, display the following:-
- Correct :: "Correct! Display(Explanation)"
- Wrong :: "Wrong Answer. Display(Correct Answer). Display(Explanation)"

** Post-test question 3 					      :scene:
  :PROPERTIES:
  :SCAT: scene
  :CUSTOM_ID: post-sc-3
  :END:

*** Question 							       :txta:
  :PROPERTIES:
  :SCAT: txta
  :CUSTOM_ID: post-q3-ques
  :END:
What is the degree of (x3−4x5+2x+1)(x2−x8+11)?

*** Options                                                            :txta:
  :PROPERTIES:
  :CUSTOM_ID: post-q3-options
  :SCAT:     txta
  :END:
a) 13
b) 40
c) 5
d) 6


*** Answer                                                             :reqa:
  :PROPERTIES:
  :CUSTOM_ID: post-q3-options
  :SCAT: reqa
  :TYPE: Text
  :END:
Correct Answer : (A)
Explanation:
Check the answer and depending on whether the answer is correct or wrong, display the following:-
- Correct :: "Correct! Display(Explanation)"
- Wrong :: "Wrong Answer. Display(Correct Answer). Display(Explanation)"

** Post-test question 4 					      :scene:
  :PROPERTIES:
  :SCAT: scene
  :CUSTOM_ID: post-sc-4
  :END:

*** Question 							       :txta:
  :PROPERTIES:
  :SCAT: txta
  :CUSTOM_ID: post-q4-ques
  :END:
Which shows the missing factor? 12(b^2) – 72b = 12b(?)

*** Options                                                            :txta:
  :PROPERTIES:
  :CUSTOM_ID: post-q4-options
  :SCAT:     txta
  :END:
 a) b^2 – 6b
 b) –b + 6
 c) 6b
 d) b – 6


*** Answer                                                             :reqa:
  :PROPERTIES:
  :CUSTOM_ID: post-q4-options
  :SCAT: reqa
  :TYPE: Text
  :END:
Correct Answer :(B)
Explanation: Degree of the difference could be atmost equal to the degree of given polynomials
Check the answer and depending on whether the answer is correct or wrong, display the following:-
- Correct :: "Correct! Display(Explanation)"
- Wrong :: "Wrong Answer. Display(Correct Answer). Display(Explanation)"

** Post-test question 5 					      :scene:
  :PROPERTIES:
  :SCAT: scene
  :CUSTOM_ID: post-sc-5
  :END:

*** Question 							       :txta:
  :PROPERTIES:
  :SCAT: txta
  :CUSTOM_ID: post-q5-ques
  :END:
What is the first term of the quotient when 27x^3 + 9x^2 - 3x – 9 is divided by 3x – 2?

*** Options                                                            :txta:
  :PROPERTIES:
  :CUSTOM_ID: post-q5-options
  :SCAT:     txta
  :END:
 a) 9(x^3)
 b) 6(x^3)
 c) 9(x^2)
 d) 6(x^2)


*** Answer                                                             :reqa:
  :PROPERTIES:
  :CUSTOM_ID: post-q5-options
  :SCAT: reqa
  :TYPE: Text
  :END:
Correct Answer : (C)
Explanation:
Check the answer and depending on whether the answer is correct or wrong, display the following:-
- Correct :: "Correct! Display(Explanation)"
- Wrong :: "Wrong Answer. Display(Correct Answer). Display(Explanation)"

** Final Feedback: 						      :scene:
  :PROPERTIES:
  :SCAT: module
  :CUSTOM_ID: ffb-scene
  :END:

*** Feedback Introduction                                              :txta:
  :PROPERTIES:
  :SCAT: txta
  :CUSTOM_ID: final-feedback-intro
  :END:
The experiment ends here,thanks for participating!
Here is a summary of your performance.

*** Final Results 						       :reqa:
  :PROPERTIES:
  :SCAT: reqa
  :CUSTOM_ID: final-performance-results
  :END:
Compute the percentage completion and marks in each module and display the final result.

*** Final Performance						       :reqa:
  :PROPERTIES:
  :SCAT: reqa
  :CUSTOM_ID: final-perform-ready
  :END:
If the performance is 100% then Congratulations! You've successfully mastered the key concepts of Linked Lists.
Above 75% : You have done well in the assessment but there is scope for improvement.You can go through the resources provided.
Below 75% : We recommend that you review the experiment once more and attempt the post-assessment once again.

*** Share 							       :reqa:
  :PROPERTIES:
  :SCAT: reqa
  :CUSTOM_ID: expt-share
  :END:
Don't forget to share the experiment so that everyone benefits from the experiment.

*** Social Media 						       :reqa:
  :PROPERTIES:
  :SCAT: reqa
  :CUSTOM_ID: expt-share-social-media
  :END:
An aretefact with options to share the experiment on diffrent social media platforms.


* Further Readings/References:		 			     :module:
  :PROPERTIES:
  :SCAT: module
  :CUSTOM_ID: fur-refer-mod
  :END:
** References							      :scene:
    :PROPERTIES:
    :SCAT: scene
    :CUSTOM_ID: refer-scene
    :END:

*** Intro							       :txta:
    :PROPERTIES:
    :SCAT: txta
    :CUSTOM_ID: references-intro
    :END:
You can explore more about linked lists through following resources.

*** Linked List Applications:					       :txta:
    :PROPERTIES:
    :SCAT: txta
    :CUSTOM_ID: ref-applications
    :END:
Linked List Applications:
